package com.example.cashinvk.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private Integer count=100;

    Toast toastStart;
    Toast toastResume;
    Toast toastPause;
    Toast toastStop;
    Toast toastRestart;
    Toast toastDestroy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toastStart = Toast.makeText(MainActivity.this, "Приложение запущено", Toast.LENGTH_SHORT);
        toastResume = Toast.makeText(MainActivity.this, "Приложение возобновлено", Toast.LENGTH_SHORT);
        toastPause = Toast.makeText(MainActivity.this, "Приложение приостановлено", Toast.LENGTH_SHORT);
        toastStop = Toast.makeText(MainActivity.this, "Приложение остановлено", Toast.LENGTH_SHORT);
        toastRestart = Toast.makeText(MainActivity.this, "Приложение перезапущено", Toast.LENGTH_SHORT);
        toastDestroy = Toast.makeText(MainActivity.this, "Приложение разрушено", Toast.LENGTH_SHORT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        toastStart.show();
        resetUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        toastResume.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        toastPause.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        toastStop.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        toastRestart.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toastDestroy.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            count = savedInstanceState.getInt("count");
        }
    }

    private void resetUI() {
        ((TextView) findViewById(R.id.counter)).setText(count.toString());
    }
}
